MJML validation is set to strict, keep an eye on the console to see compile errors.

## Gulp tasks
`compile`: compile mjml  
`watch`: *(default)* browsersync  
`imagemin`: optimize images  
`test`: build litmus test files
`preview`: optimize images, compile mjml, upload to FTP  
`build`: optimize images, compile mjml, and zip into /dist folder  


This might come in handy:
~~~~
.phone-number-link {
    text-decoration: none;
    pointer-events: none;
}
@media only screen and (max-width: 599px) {
    .phone-number-link {
        text-decoration: underline !important;
        pointer-events: all !important;
    }
}
~~~~