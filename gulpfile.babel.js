'use strict';

import fs 			from 'fs'
import path 		from 'path'
import mjmlEngine 	from 'mjml'
import browserSync 	from 'browser-sync'
import open         from 'open'
import merge        from 'merge-stream'
import gulp 		from 'gulp'
import mjml 		from 'gulp-mjml'
import plumber 		from 'gulp-plumber'
import imagemin 	from 'gulp-imagemin'
import sizereport   from 'gulp-sizereport'
import zip 			from 'gulp-zip'
import index		from 'gulp-index'
import replace      from 'gulp-replace'
import ftp          from 'vinyl-ftp'

// preview server details
const server = {
    projectName: 'Mopar Quarterly',
    baseFolder: 'FCA/CRM',
    domain: 'preview.publicis.ca',
    host: '167.246.44.216',
    user: 'previewftp',
    password: 'ce5aw&thuTuS'
}

// analytics snippets
const gaSnippet = '<img src = "https://www.google-analytics.com/collect?v=1&tid=UA-1341196-19&cid=555&aip=1&t=event&ec=email&ea=open&dp=%2Femail&dt=STDCAD" />'
const litmusSnippet = `<style data-ignore-inlining>@media print{ #_t { background-image: url('https://fq9haiaa.emltrk.com/fq9haiaa?p&d=%%email%%');}} div.OutlookMessageHeader {background-image:url('https://fq9haiaa.emltrk.com/fq9haiaa?f&d=%%email%%')} table.moz-email-headers-table {background-image:url('https://fq9haiaa.emltrk.com/fq9haiaa?f&d=%%email%%')} blockquote #_t {background-image:url('https://fq9haiaa.emltrk.com/fq9haiaa?f&d=%%email%%')} #MailContainerBody #_t {background-image:url('https://fq9haiaa.emltrk.com/fq9haiaa?f&d=%%email%%')}</style><div id="_t"></div>
<img src="https://fq9haiaa.emltrk.com/fq9haiaa?d=%%email%%" width="1" height="1" border="0" alt="" />`

// index page options
const indexOpts = {
    'relativePath': './src',
    'prepend-to-output': () => `<head> <style>body{background: #D52536; background-image: url(pub.svg); background-repeat: no-repeat; background-position: center; background-size: auto 70%; font-family: system-ui; color: #fff; font-weight: 500; font-size: 16px; padding: 40px; line-height: 1em;}a{color: #fff;}</style></head><body>`,
    'title': server.projectName,
    'section-heading-template': () => '',
    'item-template': (filepath, filename) => `<li class="index__item"><a class="index__item-link" href="${filepath}/${filename}">${filepath}</a></li>`
}

const getFolders = (dir) => {
    return fs.readdirSync(dir)
		.filter((file) => {
			return fs.statSync(path.join(dir, file)).isDirectory()
		})
}

const compileFolder = (folder) => {
    return function() {
        return gulp.src(`src/${folder}/*.mjml`)
            .pipe(plumber())
            .pipe(mjml(mjmlEngine, { minify: true, keepComments: false, validationLevel: 'strict' }))
            .pipe(replace('</body>', `${litmusSnippet}${gaSnippet}</body>`))
            .pipe(gulp.dest(`./src/${folder}`))
    }
}

const compileAll = (cb) => {
    getFolders('src').forEach((folder) => {
        compileFolder(folder)()
    })
    cb()
}

const list = () => {
    return gulp.src('./src/*/*.html')
        .pipe(sizereport({'*': {'maxSize': 80000}}))
		.pipe(index(indexOpts))
		.pipe(gulp.dest('./src'))
}

const reload = (cb) => {
    browserSync.reload()
    cb()
}

const watch = () => {
	browserSync.init({ server: 'src', baseDir: './src' })
    
    gulp.watch(['src/*', '!src/index.html'], gulp.series(list, reload))
    let watch = getFolders('src').forEach((folder) => {
        gulp.watch(`src/${folder}/*.mjml`, gulp.series(compileFolder(folder), reload))
    })
}

const images = () => {
	return gulp.src('src/**/*.+(jpg|png|gif)')
		.pipe(imagemin({ verbose: false }))
		.pipe(gulp.dest((file) => {
			return file.base
		}))
}

const compress = () => {
	let zipFiles = getFolders('src').map((folder) => {
	    return gulp.src(path.join('src', folder, '/**/*.+(html|jpg|png|gif)'))
            .pipe(zip(`${folder}.zip`))
            .pipe(gulp.dest('dist'))
        })
    let zipSize = gulp.src('./dist/*')
        .pipe(sizereport())

    return merge(zipFiles, zipSize)
}

const test = () => {
    let html = getFolders('src').map((folder) => {
        return gulp.src(path.join('src', folder, '*.html'))
            .pipe(replace('src="img/', `src="http://${server.domain}/${server.baseFolder}/${server.projectName}/${folder}/img/`))
            .pipe(gulp.dest(`./test/${folder}`))
    })
    return merge(html)
}

const previewUpload = () => {
    const conn = ftp.create({
        host: server.host,
        user: server.user,
        password: server.password
    })
    let indexUpload = gulp.src(['./src/index.html', './src/pub.svg'])
        .pipe(conn.dest(`${server.baseFolder}/${server.projectName}/`))
    let htmlUpload = getFolders('src').map((folder) => {
        return gulp.src(path.join('src', folder, '*.html'))
            .pipe(conn.dest(`${server.baseFolder}/${server.projectName}/${folder}`))
    })
    let imgUpload = getFolders('src').map((folder) => {
        return gulp.src(`./src/${folder}/img/*`)
            .pipe(conn.dest(`${server.baseFolder}/${server.projectName}/${folder}/img`))
    })
    return merge(indexUpload, htmlUpload, imgUpload)
}

const previewOpen = (cb) => {
    open(`http://${server.domain}/${server.baseFolder}/${server.projectName}`)
    cb()
}

const compile = gulp.parallel(list, compileAll)
const preview = gulp.series(gulp.parallel(list, compileAll, images), previewUpload, previewOpen)
const build = gulp.series(gulp.parallel(list, compileAll, images), compress)

export { compile, watch, images, test, preview, build }
export default watch